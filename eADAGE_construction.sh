
##############################################
# Instructions for building an eADAGE model
# This assumes you have the input file ready, please follow
# ./data_collection/data_collection.sh to build the pseudomonas gene expression
# compendium or a compendium for other microarray platform

##############################################
# Install required python packages
pip install -r python_requirements.txt

##############################################
# Specify parameters
skip_col=0  # the number of column to skip in the input file
net_structure=300  # the number of nodes in the final model
batch_size=10  # the size of a training batch
epoch_size=500  # the size of a training epoch
corrupt_level=0.1  # the percentage of noise to add
learn_rate=0.01  # the learning rate
model_num=100  # the number of individual ADAGE to train and built on
random_seed=123  # the random seed used in eADAGE construction
input_file="./data_collection/all-pseudomonas-gene-normalized.pcl"  # the input file to ADAGE
indi_output_path="./individual_ADAGE/"  # the folder that stores individual ADAGE outputs
scratch_folder="/global/scratch/jietan/"  # the folder used to store some temporary files
# Create the output file path if it does not exist
if [ ! -d $indi_output_path ];
then
    mkdir $indi_output_path;
fi

##############################################
# Generate 100 individual ADAGE
# Please be aware that the process of constructing 100 individual ADAGE models
# is highly time consuming and we highly recommend that you configure each run
# into a job and distribute them across a computing cluster.

for ((i=1;i<=model_num;i++));
do
    python ./netsize_evaluation/ADAGE_train.py $input_file $skip_col \
    $net_structure $batch_size $epoch_size $corrupt_level $learn_rate \
    $indi_output_path --seed1 $i --seed2 $i;
done

##############################################
# Build eADAGE

# Calculate the weighted Pearson correlation between each weight vector pair in 100 models
# Run overnight.
python ./ensemble_construction/weighted_pearson_correlation_parallel.py \
$indi_output_path weighted_cor.txt 1,$model_num
# Consensus clustering of 100 individual models,
# The ensemble construction is done on a 64-core machine. Run overnight.
processors=60  # the number of processors to use
module load mpich3 # load the mpi module into the environment
mpiexec -np $processors Rscript ./ensemble_construction/ensemble_construction.R \
$input_file $indi_output_path $scratch_folder $net_structure 1 \
$model_num $random_seed weighted weighted_cor.txt

