'''
Remove controls on the microarray chip and only keep valid PAO1 genes.
Any probes that start with 'Pae', 'ig', or 'AFFX' will be removed.
Probes labeled as 'PAID_name' will be converted to 'PAID'.

Usage:
    python remove_control.py in_file out_file

    in_file: pcl file with probe names and gene IDs
    out_file: output file after removing controls
'''

import sys


def remove_control(in_file, out_file):

    in_fh = open(in_file, 'r')
    out_fh = open(out_file, 'w')
    out_fh.write(in_fh.readline())  # write header
    for line in in_fh:
        toks = line.strip().split('\t')
        gid = toks.pop(0).split('_')[0]
        if gid == 'Pae' or gid == 'ig' or gid.startswith('AFFX'):
            continue  # skip controls/etc
        else:
            out_fh.write(gid + '\t' + '\t'.join(toks) + '\n')
    out_fh.close()
    in_fh.close()

if __name__ == "__main__":
    in_file = sys.argv[1]
    out_file = sys.argv[2]
    remove_control(in_file, out_file)
