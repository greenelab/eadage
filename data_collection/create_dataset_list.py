'''
This script creates a file that stores each dataset's name and the samples
contained in that dataset.

Usage:
    python create_dataset_list.py pcl_folder out_file

    pcl_folder: the folder that stores pcl files for all datasets
    out_file: a tab-delimited file with the first column storing dataset name
              and second column storing all samples in that dataset separated 
              by comma.
'''

import os
import sys

pcl_folder = sys.argv[1]
out_file = sys.argv[2]

out_fh = open(out_file, 'w')

pcl_list = os.listdir(pcl_folder)
for file in pcl_list:
    pcl_fh = open(os.path.join(pcl_folder, file), 'r')
    header = pcl_fh.readline()
    samples = header.strip().split('\t')
    out_fh.write(file.strip('.pcl')+'\t'+';'.join(samples)+'\n')
