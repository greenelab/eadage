#!/bin/bash

###########################################
# build pseudomonas expression compendium #
###########################################
# Install required python packages
pip install -r ../python_requirements.txt

# Download pseudonomas datasets from ArrayExpress
platform='A-AFFY-30'
organism='pseudomonas aeruginosa'
echo "Downloading data from ArrayExpress..."
python download_raw_data_files.py $platform $organism ./zips

# Unzip samples in all datasets into one folder and also unzip samples in one
# dataset into individual folders.
echo "Unzipping files..."
mkdir -p ./cels/all-pseudomonas
for x in ./zips/*; do unzip -n $x -d ./cels/all-pseudomonas; done
for x in ./zips/*; do mkdir -p ./cels/`basename -s .raw.1.zip $x`; unzip -n $x \
-d ./cels/`basename -s .raw.1.zip $x`; done

# Process each dataset into pcl file. Also process samples in all datasets into
# one expression compendium.
echo "Processing raw data..."
mkdir -p ./pcls/
for x in ./cels/*; do R --no-save --args $x ./pcls/`basename $x`.pcl < \
./process_to_pcl.R; done

# Create a file to store all datasets names and their sample names
python ./create_dataset_list.py ./pcls/ all-pseudomonas_SampleList.txt

# Remove controls and only keep genes starting with PA. The first argument
# takes input file (combined expression compendium) and second argument
# specifies output file.
python ./remove_control.py ./pcls/all-pseudomonas.pcl ./all-pseudomonas-gene.pcl

# To prepare for DA training, each gene expression vector is linearly normalized
# to the range between 0 and 1.
python ./zero_one_normalization.py ./all-pseudomonas-gene.pcl \
./all-pseudomonas-gene-normalized.pcl None
echo "Data collection and processing are finished!"
