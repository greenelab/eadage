"""
Convert gene name to PA ID in a file.

Usage:
    python genemae_to_PAid.py input_file output_file map_file_name

    input_file: a tab-delimited file with gene name in the first column
    output_file: the same file with gene names converted to PAIDs
    map_file_name: can be either "ncbi" or "pseudo"

"""
import sys

'''
The following two gene name-PAID mapping files are downloaded from two places.
They are both tab-delimated and included in the repository.
pseudo_map_file is downloaded from http://www.pseudomonas.com/strain/download
ncbi_map_file is downloaded from
ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/GENE_INFO/Archaea_Bacteria/ but is an old
version.
'''
pseudo_map_file = "./Pseudomonas_aeruginosa_PAO1_107.txt"
ncbi_map_file = "./Pseudomonas_aeruginosa_PAO1.gene_info"

# load gene name - PA number mapping from pseudomonas.com into a dictionary
name_PAID_pseudo = {}  # key:value = geneName:PAID
gi_fh = open(pseudo_map_file)
for i in xrange(3):
    gi_fh.next()  # skip comment and header lines
for line in gi_fh:
    toks = line.strip().split('\t')
    PAid = toks[1]  # PAID is stored in the 2nd column in this mapping file
    name = toks[6]  # gene name is stored in the 7th column
    if name == '':
        name = PAid
    name_PAID_pseudo[name] = PAid
gi_fh.close()

# load gene name - PA number mapping from NCBI into a dictionary
name_PAID_ncbi = {}  # key:value = geneName:PAID
gi_fh = open(ncbi_map_file)
gi_fh.next()  # skip header
for line in gi_fh:
    toks = line.strip().split('\t')
    name = toks[2]  # gene name is stored in the 3rd column
    PAid = toks[3]  # PAID is stored in the 4th column
    name_PAID_ncbi[name] = PAid
gi_fh.close()


def rename_file(in_file, out_file, map_file='pseudo'):

    in_fh = open(in_file, 'r')
    out_fh = open(out_file, 'w')
    out_fh.write(in_fh.readline())
    # choose mapping dictionary based on map_file
    if map_file == 'ncbi':
        dic = name_PAID_ncbi
    elif map_file == 'pseudo':
        dic = name_PAID_pseudo
    else:
        print "no such mapping file!"
    for line in in_fh:
        toks = line.strip().split('\t')
        gene_name = toks[0].strip()
        try:
            PAid = dic[gene_name]
        except KeyError:
            PAid = gene_name
            print gene_name+' not converted!'
        out_fh.write(PAid + '\t' + '\t'.join(map(lambda x: x.strip(),
                                             toks[1:])) + '\n')

    out_fh.close()
    in_fh.close()

if __name__ == "__main__":
    input_file = sys.argv[1]
    out_file = sys.argv[2]
    map_file = sys.argv[3]

    rename_file(input_file, out_file, map_file)
