
#############################
# process microarray testset #
#############################
# keep a record of the quantile distribution when normalizing the compendium
Rscript record_quantile_distribution.R cels/all-pseudomonas/ \
compendium_quantile_distribution.txt
# this is a list of microarray testset folder names
microarray_testsets=("Anr" "Ethanol")
for i in "${microarray_testsets[@]}";
do
    # use the same distribution to quantile normalize testsets
    Rscript process_microarray_testset.R testsets/$i/ \
    compendium_quantile_distribution.txt testsets/$i.txt;
    # Remove controls and only keep genes starting with PA
    python remove_control.py testsets/$i.txt testsets/${i}_gene.txt;
    # 0-1 normalize the testset using the same minimums and ranges in the
    # compendium
    python zero_one_normalization.py testsets/${i}_gene.txt \
    testsets/${i}_gene_normalized.txt all-pseudomonas-gene.pcl;

done

##########################
# process RNAseq testset #
##########################
# Since RNA-seq datasets have various formats, they need some pre-processings
# in different ways. The goal is to make each testset labeled with PA numbers.

# If samples in a dataset are stored in separate files, we use
# preprocess_separate_rnaseq.R to combine all files in a dataset folder
# together. If the strain is PA14, then the PA14 gene identifiers need to be
# converted to PAO1 gene identifiers
# separate_datasets stores a list of RNAseq folders to be preprocessed, they
# are downloaded from ArrayExpress
separate_datasets=("E-GEOD-55197.processed.1" "E-GEOD-64056.processed.1" \
    "E-GEOD-58390.processed.1")
# is_PA14 indicates whether the above dataset uses strain PA14(TRUE) or PAO1(FALSE)
is_PA14=("TRUE" "TRUE" "FALSE")
for ((i=0;i<${#separate_datasets[@]};++i));
do
    Rscript preprocess_separate_rnaseq.R ./testsets/${separate_datasets[i]}/ \
    ./testsets/${separate_datasets[i]}_PAID.txt ${is_PA14[i]};
done

# For datasets that already combined samples into one file, we just need to
# make sure the gene identifiers are PA numbers
# If a dataset uses PAO1 gene names as gene identifiers, we need to convert
# gene names to PA numbers. For gene names that cannot be mapped to PA numbers
# automatically, it would require some manual works to resolve their mappings.
# In this case, speA_1 is converted to PA4389 and speA_2 is converted to PA4839.
python genename_to_PAid.py \
testsets/MvaT-Anr-RNAseq/NormalizedExpressionValues_PAO1.txt \
testsets/NormalizedExpressionValues_PAO1_PAID.txt ncbi
python genename_to_PAid.py \
testsets/MvaT-Anr-RNAseq/NormalizedExpressionValues_dh215.txt \
testsets/NormalizedExpressionValues_dh215_PAID.txt ncbi

# RNAseq_files stores all RNAseq files that need the final TDM and 0-1
# normalization.
RNAseq_files=("E-GEOD-55197.processed.1_PAID" "E-GEOD-64056.processed.1_PAID" \
    "E-GEOD-58390.processed.1_PAID" "NormalizedExpressionValues_PAO1_PAID" \
    "NormalizedExpressionValues_dh215_PAID")
for i in "${RNAseq_files[@]}";
do
    # process RNAseq data using TDM https://github.com/greenelab/TDM
    Rscript process_rnaseq_testset.R testsets/${i}.txt all-pseudomonas-gene.pcl \
    testsets/${i}_TDMed.txt;
    # 0-1 normalize the testset using the same minimums and ranges in the
    # compendium
    python zero_one_normalization.py testsets/${i}_TDMed.txt \
    testsets/${i}_TDMed_normalized.txt all-pseudomonas-gene.pcl;
done
