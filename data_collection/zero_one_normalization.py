'''
Linearly scale the expression range of each gene to be between 0 and 1.
If a reference dataset is provided, then the scaling of a gene in the
target dataset in done using the minimum and range of that gene in the
reference dataset.

Usage:
    python zero_one_normalization.py tar_file out_file ref_file
    python zero_one_normalization.py tar_file out_file

    tar_file: the target file to do zero-one normalization
    out_file: the output file after zero-one normalization
    ref_file: optional, if provided, then the scaling of a gene in the
              target file in done using the minimum and range of that
              gene in the reference file.
'''
import sys
from pcl import PCLfile


def zero_one_normal(tar=None, out=None, ref=None):
    '''
    tar: the target file for zero one normalization
    out: the output file after zero one normalization
    ref: the reference file. If reference file is 'None',
         then zero one normalization will be done based on
         target file itself.
    '''

    if ref == 'None':
        tar_data = PCLfile(tar, skip_col=0)
        tar_data.zero_one_normalization()
        tar_data.write_pcl(out)
    else:
        ref_data = PCLfile(ref, skip_col=0)
        tar_data = PCLfile(tar, skip_col=0)
        for i in xrange(ref_data.data_matrix.shape[0]):
            row_minimum = ref_data.data_matrix[i, :].min()
            row_maximum = ref_data.data_matrix[i, :].max()
            row_range = row_maximum - row_minimum
            tar_data.data_matrix[i, :] = (tar_data.data_matrix[i, :] -
                                          row_minimum)/row_range
            # bound the values to be between 0 and 1
            tar_data.data_matrix[i, :] = [0 if x < 0 else x for x in
                                          tar_data.data_matrix[i, :]]
            tar_data.data_matrix[i, :] = [1 if x > 1 else x for x in
                                          tar_data.data_matrix[i, :]]
        tar_data.write_pcl(out)

if __name__ == "__main__":
    tar_file = sys.argv[1]
    out_file = sys.argv[2]
    ref_file = sys.argv[3]
    zero_one_normal(tar=tar_file, out=out_file, ref=ref_file)
