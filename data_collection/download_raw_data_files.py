# By Rene Zelaya
#
# Downloads all raw data files from ArrayExpress using
# a given platform

import requests
import sys
import os
import tempfile
import shutil

# Import and set logger
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def download_files_by_platform(platform, species, target_folder):
    """
    Takes a platform accession code (i.e. A-AFFY-1) and makes a new folder
    in the target folder where all raw files from this platform will be saved
    to as they are downloaded from arrayexpress. This function uses the
    'tempfile' python library and does not overwrite folders or files if they
    already exist.

    Arguments:
    platform -- A string of the platform's accession code (i.e. A-AFFY-1)
    target_folder -- A string. Name of the folder in which a new folder will be
    created (this new folder's name will be the platform's accession code), and
    where all of the downloaded files will be saved to

    Returns:
    Nothing, just downloads and saves files to new folder in target folder

    """

    # Files API endpoint for ArrayExpress
    FILES_URL = "http://www.ebi.ac.uk/arrayexpress/json/v2/files"

    parameters = {'raw': 'true', 'array': platform, 'species': species}

    r = requests.get(FILES_URL, params=parameters)
    response_dictionary = r.json()

    # The response is set up in a particular way by ArrayExpress:
    # The 'files' key is actually the only key in the json object that is
    # returned. Its value is another json object with the following keys:
    # "version" (i.e. 1.1), "revision" (i.e. 100915), "total-experiments"
    # (i.e. 11), and "experiment", where almost all of the response content is.
    # The value for the "experiment" key is a list of experiment objects
    # (which correspond to Python dictionaries).
    try:
        experiments = response_dictionary['files']['experiment']
    except KeyError:  # If the platform does not exist or has no files...
        logger.info('No files were found with this platform accession code ' +
                    'for this species. Try another accession code.')
        sys.exit(1)

    logger.info('Making directory ' + target_folder + '...')
    try:
        os.mkdir(target_folder)
    except OSError:
        logger.info('Folder ' + target_folder + ' already exists. ' +
                    'Saving downloaded files to this folder.')

    raw_file_urls = set([])

    for experiment in experiments:
        data_files = experiment['file']

        # If there is only one file object in data_files, ArrayExpress does not
        # put it in a list of size 1 - This breaks the code if we attempt to
        # iterate over it like a list. The next section handles both cases.

        if (type(data_files) == list):
            for data_file in data_files:
                if (data_file['kind'] == 'raw'):
                    url = data_file['url'].replace("\\", "")
                    raw_file_urls.add(url)

        else:  # It is just one file object
            if (data_files['kind'] == 'raw'):
                url = data_files['url'].replace("\\", "")
                raw_file_urls.add(url)

    logger.info('The total number of raw data files available to download ' +
                'for this platform is: ' + str(len(raw_file_urls)))

    # This next part was included in case some of the files have already been
    # downloaded for this platform

    files_to_download = set([])

    # This loop checks which files have already been downloaded
    for url in raw_file_urls:
        filename = url.split('/')[-1]
        target_filename = target_folder + '/' + filename

        if os.path.exists(target_filename):
            logger.info('Skipping ' + target_filename +
                        ' as it already exists in the target folder.')
        else:
            files_to_download.add(url)

    already_downloaded = len(raw_file_urls) - len(files_to_download)
    logger.info(str(already_downloaded)+' of the '+str(len(raw_file_urls)) +
                ' available files have already been downloaded.')

    file_num = 1
    for url in files_to_download:
        logger.info('Now downloading file number ' + str(file_num) + ' of ' +
                    str(len(files_to_download)))

        # In case the downloading process gets interrupted, a dummy tempfile is
        # created in the target_folder for every file that is being downloaded.
        # This tempfile is then erased once the file finishes downloading. This
        # way the user will know what zip file did not finish downloading and
        # can then erase the portion of it that has been saved so that the
        # whole file can be downloaded again.
        filename = url.split('/')[-1]
        target_filename = target_folder + '/' + filename
        temp = tempfile.NamedTemporaryFile(prefix=filename + '.',
                                           dir=target_folder)

        download_request = requests.get(url, stream=True)

        # chunk_size is in bytes
        for chunk in download_request.iter_content(chunk_size=4096):
            if chunk:
                temp.write(chunk)
                temp.flush()

        # Go back to the beginning of the tempfile and copy it to target folder
        temp.seek(0)
        target_fh = open(target_filename, 'w+b')
        shutil.copyfileobj(temp, target_fh)
        temp.close()  # This erases the tempfile
        file_num += 1


if __name__ == "__main__":
    array_platform = sys.argv[1]
    species = sys.argv[2]
    target_folder = sys.argv[3]
    download_files_by_platform(array_platform, species, target_folder)
