# The following analyses are used to help interpret the biological meaning of
# nodes in the final eADAGE model.

################################################################################
# load constants

eADAGE_model="../ensemble_construction/ensemble_ADAGE/\
net300_100models_1_100_k=300_seed=123_ClusterByweighted_avgweight_network_ADAGE.txt"
data_compendium="../data_collection/all-pseudomonas-gene-normalized.pcl"
expression_data="../data_collection/all-pseudomonas-gene.pcl"
sample_list="../data_collection/all-pseudomonas_SampleList.txt"
KEGG_term="../netsize_evaluation/pseudomonas_KEGG_terms.txt"
GO_term="../netsize_evaluation/manual_GO_BP_terms.txt"
HW_activity="$(basename -s .pcl $data_compendium)_HWActivity_perGene_with_\
$(basename $eADAGE_model)"
model_size=300
HW_cutoff=2.5

################################################################################
# extract HW genes from the final eADAGE model
Rscript write_HWG.R $eADAGE_model $HW_cutoff $data_compendium \
final_eADAGE_net300_std${HW_cutoff}_pos_neg_HWGs.txt ./HWG_lists

# calculate each node's HW activity per gene for all samples in the compendium
# and plot each node's HW activity distribution
Rscript write_HWactivity.R $eADAGE_model $HW_cutoff $data_compendium \
$HW_activity TRUE ./HW_activity_distribution

# evaluate the relationship between positive signature and negative signature
# derived from the same node
Rscript side_evaluation.R $HW_activity 50

################################################################################
# KEGG and GO pathway enrichment analysis for the final eADAGE model

Rscript pathway_enrichment_analysis.R $eADAGE_model $KEGG_term $data_compendium \
final_eADAGE_KEGG F $HW_cutoff
Rscript pathway_enrichment_analysis.R $eADAGE_model $GO_term $data_compendium \
final_eADAGE_GO F $HW_cutoff

################################################################################
# Enrichment analysis for a specific curated gene set

geneSet='phoB_regulon.txt'
geneSet_name='phoB'

# Do enrichment analysis using the curated gene set for the final eADAGE model
python find_enriched_signature.py ./HWG_lists/ $data_compendium $model_size \
./gene_sets/$geneSet gene_sets/${geneSet_name}_enrichment.txt

################################################################################
# make node activity heatmaps and gene expression heatmaps

# plot HW activity heatmaps for each dataset in the compendium with all nodes
# and for each node with all experiments
Rscript make_HW_activity_heatmaps.R $eADAGE_model $data_compendium $sample_list \
geneN-normed_HW_activity_heatmap_per_experiment \
geneN-normed_HW_activity_heatmap_per_node $HW_cutoff

# plot gene expression heatmaps of a node's HW genes for all experiments in the
# compendium
Rscript make_HWG_expression_heatmaps.R $eADAGE_model $expression_data \
compendium HWG_expression_heatmaps all $HW_cutoff FALSE

################################################################################
# make heatmaps for test sets

# the order in testset_name, testset_normed, testset_unnormed should be kept
# the same
testset_name=("Anr" "Ethanol" "E-GEOD-55197" "E-GEOD-58390" "E-GEOD-64056" \
    "Anr_RNAseq_dh215" "Anr_RNAseq_PAO1")

# testsets after 0-1 normalization are used to calculate HW activity
testset_normed=("Anr_gene_normalized.txt" "Ethanol_gene_normalized.txt" \
    "E-GEOD-55197.processed.1_PAID_TDMed_normalized.txt" \
    "E-GEOD-58390.processed.1_PAID_TDMed_normalized.txt" \
    "E-GEOD-64056.processed.1_PAID_TDMed_normalized.txt" \
    "NormalizedExpressionValues_dh215_PAID_TDMed_normalized.txt" \
    "NormalizedExpressionValues_PAO1_PAID_TDMed_normalized.txt")

# testsets before 0-1 normalization are used for plotting expression heatmaps
testset_unnormed=("Anr_gene.txt" "Ethanol_gene.txt" \
    "E-GEOD-55197.processed.1_PAID_TDMed.txt" \
    "E-GEOD-58390.processed.1_PAID_TDMed.txt" \
    "E-GEOD-64056.processed.1_PAID_TDMed.txt" \
    "NormalizedExpressionValues_dh215_PAID_TDMed.txt" \
    "NormalizedExpressionValues_PAO1_PAID_TDMed.txt")

# note: i starts from 0 in bash and 1 in zsh
for ((i=0;i<${#testset_name[@]};++i));
do
    #plot HW activity heatmaps for testsets
    Rscript make_HW_activity_heatmaps_testset.R $eADAGE_model \
    ../data_collection/testsets/${testset_normed[i]} ${testset_name[i]} \
    $HW_activity geneN-normed_HW_activity_heatmap_per_experiment/testset \
    geneN-normed_HW_activity_heatmap_per_node/testset all $HW_cutoff;

    #plot expression heatmaps of HW genes for testsets (one heatmap per node)
    Rscript make_HWG_expression_heatmaps.R $eADAGE_model \
    ../data_collection/testsets/${testset_unnormed[i]} ${testset_name[i]} \
    HWG_expression_heatmaps/testsets all $HW_cutoff FALSE;
done


