'''
This script is used to find signature whose genes are over-represented
in a gold standard gene list (a biological pathway or a TF's downstream
targets).

Usage:
    find_enriched_signature.py <HWG_folder> <data_file> <net_size> <gold_std>
    <out_file>
    find_enriched_signature.py -h | --help


Options:
    -h --help       Show this screen.
    <HWG_folder>:   the folder that stores high-weight gene files for each
                    feature
    <data_file>:    the microarray file with gene expression values
    <net_size>:     the number of nodes in the model
    <gold_std:      the gold standard file that contains a list of genes>
    <out_file>:     the output file that stores each signature and its
                    corresponding q value
'''


from scipy import stats
from docopt import docopt
from statsmodels.stats.multitest import multipletests
import pandas as pd
import sys
import os
sys.path.insert(0, '../data_collection/')  # for importing PCLfile
from pcl import PCLfile


def contingency_table(gene_list, gold_list, all_gene):
    '''
    This function builds a contingency table using gold_list as truth and
    gene_list as prediction. all_gene contains all possible genes. It returns
    the contingency table.

    Input:
    gene_list: list, contains genes in a specific gene set for testing
    gold_list: list, contains genes in a gold standard gene set (such as genes
              in a biological pathway)
    all_gene: list, contains all possible genes (all genes in the compendium)

    Output:
    table: a 2D list, the resulting contingency table
    '''

    all_overlap_genes = set(gold_list).intersection(set(all_gene))
    selected_overlap_genes = set(gold_list).intersection(set(gene_list))
    a = len(selected_overlap_genes)
    b = len(all_overlap_genes) - len(selected_overlap_genes)
    c = len(gene_list) - len(selected_overlap_genes)
    d = len(all_gene) - a - b - c
    table = [[a, b], [c, d]]

    return table


def test_one_signature(signature_file, gold_list, all_gene):
    '''
    This function performs an enrichment test for one signature.

    Input:
    signature_file: file path to a signature file
    gold_list: list, contains genes in a gold standard gene set (genes in a
              biological pathway)
    all_gene: list, contains all possible genes (all genes in the compendium)

    Output:
    pvalue: the p value of the enrichment test
    '''

    if os.stat(signature_file).st_size == 0:
        # return 1 if the signature_file is empty, which means there is no gene
        # in this signature
        return 1

    else:
        # read in genes in a signature (one HW side of a node)
        gene_df = pd.read_table(signature_file, header=None)
        gene_list = gene_df[0].tolist()

        # build the contingency table
        table = contingency_table(gene_list, gold_list, all_gene)

        # calculate p-value using fisher exact test
        oddsratio, pvalue = stats.fisher_exact(table)
        return pvalue


def find_enriched_signature(geneList_folder=None, data_file=None, net_size=300,
                            gold_std=None, out_file=None):
    '''
    This function finds the signature in a model most significantly enriched of
    a given gold standard gene set.

    Input:
    geneList_folder: the folder that stores high-weight gene files for each
                     signature, each file contains genes in a signature with
                     one gene per line without header
    data_file: the microarray file with gene expression values
    net_size: the number of nodes in the model
    gold_std: the gold standard file that contains a list of genes
    out_file: the output file that stores each signature and its corresponding
              q value
    '''

    datasets = PCLfile(data_file, skip_col=0)
    all_gene = datasets.id_list

    gold_data = pd.read_table(gold_std, header=None, index_col=0)
    gold_list = gold_data.index

    all_pvalues = []
    all_signatures = []
    for i in xrange(net_size):

        # positive side of a node
        # a signature is one high-weight side of a node
        signature = 'Node' + str(i + 1) + "pos"
        all_signatures.append(signature)
        signature_file = os.path.join(geneList_folder, signature + '.txt')
        pvalue = test_one_signature(signature_file, gold_list, all_gene)
        all_pvalues.append(pvalue)

        # negative side of a node
        signature = 'Node' + str(i + 1) + "neg"
        all_signatures.append(signature)
        signature_file = os.path.join(geneList_folder, signature + '.txt')
        pvalue = test_one_signature(signature_file, gold_list, all_gene)
        all_pvalues.append(pvalue)

    # multiple hypothesis correction
    result_adj_pvalue = multipletests(all_pvalues, alpha=0.05,
                                      method='fdr_bh')[1]

    # sort by q value
    signature_qvalue = zip(all_signatures, result_adj_pvalue)
    signature_qvalue = sorted(signature_qvalue,
                              key=lambda signature_qvalue: signature_qvalue[1])
    print (signature_qvalue[0][0] + ' is most significantly associated with ' +
           'this gene set with a q value of ' + str(signature_qvalue[0][1]))

    # write all signatures and their associated q values into output
    signature_qvalue = pd.DataFrame(signature_qvalue,
                                    columns=['feature', 'adjusted p-value'])
    signature_qvalue.to_csv(out_file, sep='\t', index=False)


if __name__ == '__main__':

    arguments = docopt(__doc__, version=None)
    HWG_folder = arguments["<HWG_folder>"]
    data_file = arguments["<data_file>"]
    net_size = int(arguments["<net_size>"])
    gold_std = arguments["<gold_std>"]
    out_file = arguments["<out_file>"]

    find_enriched_signature(geneList_folder=HWG_folder, data_file=data_file,
                            net_size=net_size, gold_std=gold_std,
                            out_file=out_file)
