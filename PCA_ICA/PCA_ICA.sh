data_compendium='../data_collection/all-pseudomonas-gene.pcl'
model_size=300
HW_cutoff=2.5
pathway_file="../netsize_evaluation/pseudomonas_KEGG_terms.txt"

#build a PCA and 10 ICA models
Rscript PCA_ICA.R $data_compendium $model_size 10

#pathway enrichment analysis for PCA and ICA models
Rscript PCA_ICA_pathway_enrichment.R $pathway_file $data_compendium $HW_cutoff

#compare PCA and ICA with eADAGE in pathway coverage and significance
Rscript compare_PCA_ICA_eADAGE.R $pathway_file
