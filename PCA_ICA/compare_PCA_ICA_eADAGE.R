###############################################################################
# This script compares the pathway coverage and significance among PCA, ICA,
# and eADAGE models
#
# Usage:
#     Rscript compare_PCA_ICA_eADAGE.R
#
###############################################################################

pacman::p_load("ggplot2", "plyr", "readr", "dplyr", "tools")

########## load in command argument

pathway_file <- commandArgs(trailingOnly = TRUE)[1]
pathway_file_base <- file_path_sans_ext(basename(pathway_file))

########## load in constant

# summary_file stores the output of this script, it will be overwritten
# in each run.
summary_file <- paste0(pathway_file_base, "compare_PCA_ICA_eADAGE_output.txt")
sig_cutoff <- 0.05

########## load in data

# load PCA pathway enrichment results
PCA_all_path <- read_delim(paste0(pathway_file_base, "_PCA_allPathway.txt"),
                           delim = "\t", col_names = T)
PCA_all_path_lowq <- ddply(PCA_all_path, .(pathway), summarize, lowq = min(qvalue))

# load ICA pathway enrichment results
ICA_folder <- "ICA_pathways"
ICA_pathways <- list.files(ICA_folder, pattern = paste0(pathway_file_base,
                                                        "_ICA_allPathway*"))
ICA_all_path <- c()
for (ICA_pathway in ICA_pathways) {
  ICA_path <- read_delim(file.path(ICA_folder, ICA_pathway), delim = "\t",
                         col_names = T)
  modelN <- strsplit(tail(unlist(strsplit(ICA_pathway, "_")), 1), "\\.")[[1]][1]
  ICA_path <- data.frame(model = rep(modelN, nrow(ICA_path)), ICA_path,
                         stringsAsFactors = F)
  ICA_all_path <- rbind(ICA_all_path, ICA_path)
}
ICA_all_path_lowq <- ddply(ICA_all_path, .(model, pathway), summarize,
                           lowq = min(qvalue))

# load eADAGE pathway enrichment results
eADAGE_pathway <- file.path("..", "ensemble_construction",
                            paste0(pathway_file_base,
                                   "_diff100_eADAGE_allPathway.txt"))
eADAGE_all_path <- read_delim(eADAGE_pathway, delim = "\t", col_names = T)
eADAGE_all_path_lowq <- ddply(eADAGE_all_path, .(model, pathway), summarize,
                              lowq = min(qvalue))

############################################################
# evaluate the pathway coverage under different cutoffs

# significance cutoffs
cutoffs <- c(0.05, 0.005, 5e-04, 5e-05, 5e-06)
ICA_eADAGE_coverage_cutoff <- c()

for (cutoff in cutoffs) {
  ICA_all_path_lowq_sig <- ICA_all_path_lowq[ICA_all_path_lowq$lowq <=
                                               cutoff, ]
  ICA_coverage <- ddply(ICA_all_path_lowq_sig, .(model), summarize,
                        count = length(model))
  eADAGE_all_path_lowq_sig <- eADAGE_all_path_lowq[eADAGE_all_path_lowq$lowq <=
                                                     cutoff, ]
  eADAGE_coverage <- ddply(eADAGE_all_path_lowq_sig, .(model), summarize,
                           count = length(model))
  ICA_eADAGE_coverage <- rbind(ICA_coverage, eADAGE_coverage)
  ICA_eADAGE_coverage$method <- c(rep("ICA", nrow(ICA_coverage)),
                                  rep("eADAGE", nrow(eADAGE_coverage)))
  ICA_eADAGE_coverage$cutoff <- rep(cutoff, nrow(ICA_eADAGE_coverage))
  ICA_eADAGE_coverage_cutoff <- rbind(ICA_eADAGE_coverage_cutoff,
                                      ICA_eADAGE_coverage)

  PCA_sig_path <- PCA_all_path[PCA_all_path$qvalue <= cutoff, ]
  PCA_coverage <- nlevels(factor(PCA_sig_path$pathway))
  # only have one PCA model
  PCA_coverage_cutoff <- c("1", PCA_coverage, "PCA", cutoff)
  ICA_eADAGE_coverage_cutoff <- rbind(ICA_eADAGE_coverage_cutoff,
                                      PCA_coverage_cutoff)
}
ICA_eADAGE_coverage_cutoff$cutoff <- factor(ICA_eADAGE_coverage_cutoff$cutoff,
                                            levels = cutoffs)
ICA_eADAGE_coverage_cutoff$count <- as.numeric(as.character(
  ICA_eADAGE_coverage_cutoff$count))

pdf(paste0(pathway_file_base, "_", "PCA_ICA_eADAGE_coverage.pdf"),
    width = 5, height = 5)
ggplot(ICA_eADAGE_coverage_cutoff, aes(x = cutoff, y = count, colour = method)) +
  geom_boxplot() + theme_bw()
dev.off()

############################################################
# evaluate pathway enrichment results between eADAGE and ICA

# combine eADAGE and ICA results
combine_ICA_eADAGE <- rbind(ICA_all_path_lowq, eADAGE_all_path_lowq)
combine_ICA_eADAGE$method <- c(rep("ICA", nrow(ICA_all_path_lowq)),
                               rep("eADAGE", nrow(eADAGE_all_path_lowq)))

pdf(paste0(pathway_file_base, "_",
           "compare_ICA_eADAGE_mostsig_per_pathway.pdf"),
    height = 200, width = 15)
ggplot(combine_ICA_eADAGE, aes(x = factor(pathway), y = -log10(lowq),
                               fill = method)) +
  geom_boxplot() + coord_flip() + theme_bw()
dev.off()

# compare pathway significance
ICA_all_path_lowq_median <- ddply(ICA_all_path_lowq, .(pathway), summarize,
                                  medianq = median(-log10(lowq)))
eADAGE_all_path_lowq_median <- ddply(eADAGE_all_path_lowq, .(pathway),
                                     summarize, medianq = median(-log10(lowq)))
eADAGE_ICA_com <- round(sum(ICA_all_path_lowq_median$medianq <
                              eADAGE_all_path_lowq_median$medianq) /
                          nrow(ICA_all_path_lowq_median) * 100, 2)
print(paste(eADAGE_ICA_com, "percentage of pathways are better captured in",
            "eADAGE than ICA."))
write(paste(eADAGE_ICA_com, "percentage of pathways are better captured in",
            "eADAGE than ICA"), summary_file)

# pathway coverage in ICA
ICA_all_path_lowq_sig <- ICA_all_path_lowq[ICA_all_path_lowq$lowq <= sig_cutoff, ]
ICA_all_path_lowq_sig_median <- ddply(ICA_all_path_lowq_sig, .(pathway),
                                      summarize, medianq = median(-log10(lowq)))
ICA_tot_path <- nrow(ICA_all_path_lowq_sig_median)
print(paste("Total number of pathways significantly enriched in 10 ICA models:",
            ICA_tot_path))
write(paste("Total number of pathways significantly enriched in 10 ICA models:",
            ICA_tot_path), summary_file, append = T)

# pathway coverage in eADAGE
eADAGE_all_path_lowq_sig <- eADAGE_all_path_lowq[eADAGE_all_path_lowq$lowq <=
                                                   sig_cutoff, ]
eADAGE_all_path_lowq_sig_median <- ddply(eADAGE_all_path_lowq_sig, .(pathway),
                                         summarize,
                                         medianq = median(-log10(lowq)))
eADAGE_tot_path <- nrow(eADAGE_all_path_lowq_sig_median)
print(paste("Total number of pathways significantly enriched in 10 eADAGE models:",
            eADAGE_tot_path))
write(paste("Total number of pathways significantly enriched in 10 eADAGE models:",
            eADAGE_tot_path), summary_file, append = T)

# compare pathway significance only for statistically significant pathways
merge_ICA_eADAGE <- full_join(ICA_all_path_lowq_sig_median,
                              eADAGE_all_path_lowq_sig_median,
                              by = "pathway", suffix = c(".ICA", ".eADAGE"))
merge_ICA_eADAGE[is.na(merge_ICA_eADAGE)] <- 0
eADAGE_ICA_sig_com <- round(sum(merge_ICA_eADAGE$medianq.ICA <
                                  merge_ICA_eADAGE$medianq.eADAGE) /
                              nrow(merge_ICA_eADAGE) * 100, 2)

print(paste("Among pathways significantly enriched in either ICA or eADAGE,",
            eADAGE_ICA_sig_com,
            "percentage of them are better captured in eADAGE than ICA."))
write(paste("Among pathways significantly enriched in either ICA or eADAGE,",
            eADAGE_ICA_sig_com,
            "percentage of them are better captured in eADAGE than ICA."),
      summary_file, append = T)

############################################################
# evaluate number of pathways per component in PCA

PCA_sig_path <- PCA_all_path[PCA_all_path$qvalue <= sig_cutoff, ]
PCA_sig_path_perNode <- plyr::count(PCA_sig_path, c("component"))
PCA_sig_path_perNode$PC <- sapply(1:nrow(PCA_sig_path_perNode),
                                  function(x) as.numeric(unlist(strsplit(
                                    PCA_sig_path_perNode[x, 1], " "))[2]))
PCA_cor <- round(cor(PCA_sig_path_perNode$PC, PCA_sig_path_perNode$freq), 2)

print(paste("In PCA, the correlation between the number of significantly",
            "enriched pathways per node and the order of PCs is", PCA_cor))
write(paste("In PCA, the correlation between the number of significantly",
            "enriched pathways per node and the order of PCs is", PCA_cor),
      summary_file, append = T)