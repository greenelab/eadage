medium_annotation="./medium_annotation_05102016.txt"
eADAGE_model="../ensemble_construction/ensemble_ADAGE/net300_100models_1_100_\
k=300_seed=123_ClusterByweighted_avgweight_network_ADAGE.txt"
expression_data="../data_collection/all-pseudomonas-gene.pcl"
data_compendium="../data_collection/all-pseudomonas-gene-normalized.pcl"
HW_activity="../node_interpretation/$(basename -s .pcl $data_compendium)_\
HWActivity_perGene_with_$(basename $eADAGE_model)"
HW_cutoff=2.5

# medium analysis
Rscript medium_analysis.R $HW_activity $medium_annotation eADAGE TRUE \
./medium_activity_plots

# plot expression heatmaps for HW genes and label samples by their medium types
Rscript ../node_interpretation/make_HWG_expression_heatmaps.R $eADAGE_model \
$expression_data medium.annotated "HWG expression heatmaps labeled by medium" \
all $HW_cutoff TRUE $medium_annotation

# compare PCA, ICA, ADAGE, and eADAGE in medium analysis and PhoB enrichment
result_folder="compare_PCA_ICA_ADAGE_eADAGE"
python compare_PCA_ICA_ADAGE.py $data_compendium $HW_cutoff $result_folder

# extract the most enriched signature in each model (in the first line of the
# phoB_enrichment.txt file) and save them into phoB_enrichment_summary.txt
phoB_results=($result_folder/*_phoB_enrichment.txt)
touch $result_folder/phoB_enrichment_summary.txt
# note: i starts from 0 in bash and 1 in zsh
for ((i=0;i<${#phoB_results[@]};++i));
do
    line="$(basename -s .txt ${phoB_results[i]})\t$(head -n 2 ${phoB_results[i]}| tail -n 1)"
    echo $line >> $result_folder/phoB_enrichment_summary.txt
done

# summarize numbers of genes in and out of the PhoB regulon and output results
# in a table
Rscript phoB_summary.R $result_folder/phoB_enrichment_summary.txt
