"""
This script processes 1 PCA model and 10 each ICA, ADAGE, and eADAGE models and
then performs medium analysis and PhoB enrichment analysis for each model.

Usage:
    compare_PCA_ICA_ADAGE.py <data_compendium> <HW_cutoff> <result_folder>
    compare_PCA_ICA_ADAGE.py -h | --help


Options:
    -h --help           Show this screen.
    <data_compendium>:  file path to the training expression compendium
    <HW_cutoff>:        number of standard deviations away from mean to be
                        considered as high-weight
    <result_folder>:    folder to save results
"""


from docopt import docopt
import subprocess
import os

# load command arguments
arguments = docopt(__doc__, version=None)
data_compendium = arguments["<data_compendium>"]
result_folder = arguments["<result_folder>"]
HW_cutoff = arguments["<HW_cutoff>"]

# there are 31 models in total, 1 PCA model and 10 ICA, ADAGE, eADAGE models
modelN = 31

# specify method type of each model
methods = ["PCA"] + ["ICA"]*10 + ["ADAGE"]*10 + ["eADAGE"]*10

# specify model path of each model
model_paths = ["../PCA_ICA/"] +\
              ["../PCA_ICA/ICA_models/"]*10 +\
              ["../netsize_evaluation/models/300/"]*10 +\
              ["../ensemble_construction/ensemble_models/"]*10

# specify model seed of each model
model_seeds = [""] + map(str, range(1, 11)) + map(str, range(1, 11)) +\
              map(str, range(1, 902, 100))

# specify file name of each model
PCA_name = ["PCA_weight_matrix.txt"]
ICA_name = ["ICA_weight_matrix_{}.txt".format(str(seed)) for seed in
            range(1, 11)]
ADAGE_name = [("all-pseudomonas-gene-normalized_300_batch10_epoch500_"
               "corrupt0.1_lr0.01_"
               "seed1_{}_seed2_{}_network_SdA.txt").format(str(seed), str(seed))
              for seed in range(1, 11)]
eADAGE_name = [("net300_100models_{}_{}_k=300_seed=1_ClusterByweighted"
                "_avgweight_network_ADAGE.txt").format(str(seed), str(seed+99))
               for seed in range(1, 902, 100)]
model_names = PCA_name + ICA_name + ADAGE_name + eADAGE_name

# create a folder to save results
if not os.path.exists(result_folder):
    os.mkdir(result_folder)

# loop through each model
for i in range(modelN):

    model_full_path = model_paths[i] + model_names[i]
    model_output_prefix = model_paths[i] + methods[i] + model_seeds[i]
    result_output_prefix = os.path.join(result_folder,
                                        methods[i] + model_seeds[i])

    print("Processing model " + model_full_path)

    # extract HW genes for each signature
    subprocess.call(("Rscript ../node_interpretation/write_HWG.R {0} {1} {2} "
                     "{3}_HWG.txt {3}_HWG_lists").format(model_full_path,
                    HW_cutoff, data_compendium, model_output_prefix),
                    shell=True)

    # calculate signature activities
    subprocess.call(("Rscript ../node_interpretation/write_HWactivity.R {0} "
                     "{1} {2} {3}_HWactivity.txt FALSE").format(model_full_path,
                    HW_cutoff, data_compendium, model_output_prefix),
                    shell=True)

    # perform medium analysis
    subprocess.call(("Rscript medium_analysis.R {0}_HWactivity.txt "
                     "./medium_annotation_05102016.txt {1} FALSE").format(
                     model_output_prefix, result_output_prefix), shell=True)

    # perform PhoB enrichment analysis
    subprocess.call(("python ../node_interpretation/find_enriched_signature.py "
                     "{0}_HWG_lists {1} 300 "
                     "../node_interpretation/gene_sets/phoB_regulon.txt "
                     "{2}_phoB_enrichment.txt").format(model_output_prefix,
                    data_compendium, result_output_prefix), shell=True)
