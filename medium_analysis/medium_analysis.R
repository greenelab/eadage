################################################################################
# This script performs several analyses centered around medium annotations:
# plotting signature activities per medium type, calculating medium activation
# score per signature per medium, and identifying signatures that are active in
# multiple media.
#
# Usage:
#     Rscript medium_analysis.R activity_file annotation_file
#
#     activity_file: file path to the activity file that stores signature
#                    activity of each sample
#     annotation_file: file path to the annotation file that stores the medium
#                      type of each sample
#     output_prefix: prefix to all outputs except the activity plots
#     activity_plot: logical, whether or not to make activity plots for all
#                    signatures
#     activity_plot_folder: folder to save activity plots
################################################################################

####### load required packages

pacman::p_load("ggplot2", "plyr")

####### load in command arguments

comArgs <- commandArgs(trailingOnly = TRUE)
activity_file <- comArgs[1]
annotation_file <- comArgs[2]
output_prefix <- comArgs[3]
activity_plot <- as.logical(comArgs[4])

if (activity_plot) {
  activity_plot_folder <- comArgs[5]
  dir.create(activity_plot_folder)
}

####### define functions

plot_media_signature_activity <- function(media, signature, merged_df, plot_fd){
  # This function makes a signature activity boxplot for the input signature
  # among the input media.

  merged_df$medium <- as.character(merged_df$medium)
  merged_df$medium[!merged_df$medium %in% media] <- "other"
  merged_df$medium <- factor(merged_df$medium)
  merged_df$medium <- relevel(merged_df$medium, ref = "other")
  ggplot(data = merged_df, aes(x = merged_df[, "medium"],
                               y = merged_df[, signature])) +
    geom_boxplot() + geom_point(size = 0.5) +
    labs(title = signature, x = "Medium", y = "Activity") + theme_bw()
  ggsave(file.path(plot_fd, paste0(paste(media, collapse = "_"), "_",
                                   signature,".pdf")),
         height = 2, width = 3, limitsize = FALSE)
}

####### specify constant

# this cutoff is determined by the distribution of the activation scores
activation_cutoff <- 0.4

####### load in data

# read in the signature activities
activity_compendium <- read.table(activity_file, header = T,
                                  row.names = 1, sep = "\t")
signatureN <- ncol(activity_compendium)
sample_names <- rownames(activity_compendium)

# read in the medium annotations
annotations <- read.table(annotation_file, sep = "\t", fill = T,
                          quote = "", header = T, stringsAsFactors = F)
# remove duplicates
annotations_noDup <- subset(annotations, !duplicated((annotations$cel_file)))
# merge signature activities and medium annotations
merged <- merge(activity_compendium, annotations_noDup, by.x = 0,
                by.y = "cel_file", all.x = F, all.y = F)
merged$medium <- trimws(merged$medium)
merged$medium <- factor(merged$medium)

#######################################################
# count number of samples/experiments in each medium

exp_medium <- ddply(merged, .(experiment, medium), summarize,
                    freq = length(medium))
count_table <- cbind(table(merged$medium), table(exp_medium$medium))
colnames(count_table) <- c("#sample", "#experiment")
write.table(count_table, "medium_count.txt", sep = "\t", row.names = T,
            col.names = NA, quote = F)

#######################################################
# plot signature activities grouped by medium types

if (activity_plot) {
  # loop through signatures that start from the second column
  for (i in 1:signatureN + 1) {
    ggplot(data = merged, aes(x = merged[, "medium"], y = merged[, i])) +
      geom_boxplot() + geom_point(size = 0.5) + coord_flip() +
      labs(x = "medium", y = colnames(merged)[i])
    ggsave(file.path(activity_plot_folder,
                     paste0(colnames(merged)[i], "_medium.pdf")),
           height = 50, width = 8, limitsize = FALSE)
  }

  # the following plots are example plots used for making a supplemental figure
  # in the eADAGE paper
  plot_media_signature_activity("NGM + <0.1  mM phosphate", "Node164pos", merged,
                                "medium_activity_plots")
  plot_media_signature_activity("NGM + <0.1  mM phosphate", "Node108neg", merged,
                                "medium_activity_plots")
  plot_media_signature_activity("Peptone", "Node164pos", merged,
                                "medium_activity_plots")
  plot_media_signature_activity("King's A", "Node164pos", merged,
                                "medium_activity_plots")
  plot_media_signature_activity(c("NGM + <0.1  mM phosphate", "Peptone",
                                  "King's A"), "Node164pos", merged,
                                "medium_activity_plots")

}


#######################################################
# calculate medium activation score per signature

all_medium <- levels(merged$medium)
all_medium_result <- list()
for (i in 1:length(all_medium)) {

  this_medium <- all_medium[i]
  score_list <- c()

  # only analyze medium with more than 1 sample
  if (nrow(merged[merged$medium == this_medium, ]) > 1) {

    # loop through signatures that start from the second column
    for (signature in 1:signatureN + 1) {
      diff <- abs(
        mean(merged[merged$medium == this_medium, signature]) -
          mean(merged[merged$medium != this_medium, signature]))
      activation_score <- diff/(max(merged[, signature]) -
                                  min(merged[, signature]))
      score_list <- c(score_list, activation_score)
    }

    this_medium_result <- data.frame(
      medium = rep(this_medium, length(score_list)),
      signature = colnames(merged)[1:signatureN + 1],
      activation_score = score_list)
    # order by activation score
    this_medium_result <- this_medium_result[
      order(this_medium_result$activation_score, decreasing = TRUE), ]
    all_medium_result[[i]] <- this_medium_result

  }
}

all_medium_result <- do.call(rbind, all_medium_result)


#######################################################
# plot the distribution of activation scores

pdf(paste0(output_prefix, "_activation_score_density.pdf"),
    height = 5, width = 5)
plot(density(all_medium_result$activation_score, na.rm = TRUE),
     main = "Distribution of medium activation scores")
dev.off()

#######################################################
# filter the result with the activation score cutoff

all_medium_result_selected <- all_medium_result[
  all_medium_result$activation_score >= activation_cutoff, ]
write.table(all_medium_result_selected,
            paste0(output_prefix,
                   "_signature_medium_activation_greaterthan_cutoff",
                   activation_cutoff, ".txt"), sep = "\t", row.names = FALSE,
            col.names = TRUE, quote = FALSE)

#######################################################
# identify signatures active for multiple media

signature_media <- ddply(all_medium_result_selected, .(signature),
                         summarize, mean_activation = mean(activation_score),
                         freq = length(medium),
                         media = paste(medium, collapse = ", "))
signature_media <- signature_media[signature_media$freq > 1, ]
signature_media <- signature_media[order(signature_media$mean_activation,
                                         decreasing = TRUE), ]
write.table(signature_media, paste0(output_prefix,
                                    "_Signature_MeanActivationScore_Media.txt"),
            sep = "\t", row.names = FALSE, col.names = TRUE, quote = FALSE)
